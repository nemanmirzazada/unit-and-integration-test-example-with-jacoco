package com.example.testingtutorial.Model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class OrderDtos {
    private BigDecimal totalPrice;
    private Integer id;

}
