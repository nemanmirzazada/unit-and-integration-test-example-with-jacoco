package com.example.testingtutorial.Repository;

import com.example.testingtutorial.Model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;


public interface OrderRepository extends CrudRepository<Orders,Long> {

}
