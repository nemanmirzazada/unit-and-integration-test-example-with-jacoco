package com.example.testingtutorial.Service;

import lombok.*;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderRequest {
    private String productCode;
    private Integer amount;
    private BigDecimal unitPrice;

}
