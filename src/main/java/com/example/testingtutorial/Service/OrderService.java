package com.example.testingtutorial.Service;

import com.example.testingtutorial.Model.Orders;
import com.example.testingtutorial.Model.OrderDtos;
import com.example.testingtutorial.Model.PaymentClient;
import com.example.testingtutorial.Repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final PaymentClient paymentClient;
    public OrderDtos createOrder(CreateOrderRequest request) {
        BigDecimal totalPrice=request.getUnitPrice().multiply(BigDecimal.valueOf(request.getAmount()));
        Orders orders = Orders.builder().totalPrice(totalPrice).build();
        this.paymentClient.pay(orders);
        Orders save=this.orderRepository.save(orders);
        return OrderDtos.builder().id(save.getId()).totalPrice(totalPrice).build();
    }
}
