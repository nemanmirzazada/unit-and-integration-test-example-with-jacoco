package com.example.testingtutorial.Repository;

import com.example.testingtutorial.Model.Orders;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

@Testcontainers
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class OrdersRepositoryTest {
    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private OrderRepository orderRepository;
    @Container
    public static MySQLContainer<?> mysql=new MySQLContainer<>("mysql");
    @Test
    public void it_should_find_orders(){
        //given
        Orders orders1 = Orders.builder().totalPrice(BigDecimal.valueOf(10)).build();
        Orders orders2 = Orders.builder().totalPrice(BigDecimal.valueOf(2)).build();

        Object id1=this.testEntityManager.persistAndGetId(orders1);
        Object id2=this.testEntityManager.persistAndGetId(orders2);
        this.testEntityManager.flush();

        //when
        Iterable<Orders> orders =this.orderRepository.findAll();

        //then
        then(orders).isNotEmpty();
        Iterator<Orders> it= orders.iterator();
        Orders o1= it.next();
        Orders o2=it.next();
        then(o1.getId()).isEqualTo(id1);
        then(o2.getId()).isEqualTo(id2);
    }

    @DynamicPropertySource
    public static void properties(DynamicPropertyRegistry registry){
        registry.add("spring.jpa.hibernate.ddl-auto",()->"create-drop");
        registry.add("spring.jpa.database-platform",()->"org.hibernate.dialect.MySQL5InnoDBDialect");
        registry.add("spring.datasource.url",mysql::getJdbcUrl);
        registry.add("spring.datasource.username",mysql::getUsername);
        registry.add("spring.datasource.password",mysql::getPassword);
        registry.add("spring.datasource.driver-class-name",mysql::getDriverClassName);
    }
}