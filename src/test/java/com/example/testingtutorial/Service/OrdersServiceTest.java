package com.example.testingtutorial.Service;

import com.example.testingtutorial.Model.Orders;
import com.example.testingtutorial.Model.OrderDtos;
import com.example.testingtutorial.Model.PaymentClient;
import com.example.testingtutorial.Repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrdersServiceTest {
    @InjectMocks
    private OrderService orderService;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private PaymentClient paymentClient;

    public static Stream<Arguments> order_requests() {
        return Stream.of(
                Arguments.of("code1",5,BigDecimal.valueOf(12.3),BigDecimal.valueOf(61.5)),
                Arguments.of("code2",10,BigDecimal.valueOf(15),BigDecimal.valueOf(150)),
                Arguments.of("code3",15,BigDecimal.valueOf(12),BigDecimal.valueOf(180))
                );
    }


    @ParameterizedTest
    @MethodSource("order_requests")
    public void it_should_create_orders(String productCode,Integer amount,BigDecimal unitPrice,BigDecimal totalPrice){
        //given
        CreateOrderRequest request= CreateOrderRequest.builder()
                .productCode(productCode)
                     .amount(amount)
                        .unitPrice(unitPrice)
                             .build();

       Orders orders =new Orders();
       orders.setId(13543);

        when(orderRepository.save(any())).thenReturn(orders);

        // when
        OrderDtos orderDto=orderService.createOrder(request);

        // then
        then(orderDto.getTotalPrice()).isEqualTo(totalPrice);
    }


    @Test
    public void it_should_fail_order_creation_when_payment_failed(){
        //given
        CreateOrderRequest request= CreateOrderRequest.builder()
                .productCode("code1")
                .amount(3)
                .unitPrice(BigDecimal.valueOf(12))
                .build();

        doThrow(new IllegalArgumentException()).when(paymentClient).pay(any());

        //when
        Throwable throwable=catchThrowable(()->{orderService.createOrder(request);});

        //then
        then(throwable).isInstanceOf(IllegalArgumentException.class);

        verifyNoInteractions(orderRepository);

    }
}